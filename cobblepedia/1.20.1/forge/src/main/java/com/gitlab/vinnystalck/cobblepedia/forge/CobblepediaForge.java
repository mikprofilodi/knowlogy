package com.gitlab.vinnystalck.cobblepedia.forge;

import com.gitlab.vinnystalck.cobblepedia.Cobblepedia;
import net.minecraftforge.fml.common.Mod;

/**
 * Main class for the mod on the Forge platform.
 */
@Mod(Cobblepedia.MOD_ID)
public class CobblepediaForge {
    public CobblepediaForge() {
        Cobblepedia.init();
    }
}
