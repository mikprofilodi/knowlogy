package com.gitlab.vinnystalck.cobblepedia.book.template;

import vazkii.patchouli.api.IVariableProvider;

public class ProcessorUtil {
    
    private static String model_item_id(String species_id) {
        String aspects = "[]";
        
        if (species_id.contains("[")) {
            aspects = species_id.substring(species_id.indexOf("["));
            species_id = species_id.substring(0, species_id.indexOf("["));
        }

        return String.format("cobblemon:pokemon_model{species:'%s', aspects:%s}", species_id, aspects);
    }

    public static String model_item_id(IVariableProvider variables, String species_key) {
        String species_id = null;

        if (variables.has(species_key)) {
            species_id = variables.get(species_key).asString();
        }

        if (species_id != null) {
            return model_item_id(species_id);
        }

        return null;
    }
}
