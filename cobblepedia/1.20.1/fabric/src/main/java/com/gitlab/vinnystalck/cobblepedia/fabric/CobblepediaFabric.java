package com.gitlab.vinnystalck.cobblepedia.fabric;

import com.gitlab.vinnystalck.cobblepedia.Cobblepedia;
import net.fabricmc.api.ModInitializer;

/**
 * This class is the entrypoint for the mod on the Fabric platform.
 */
public class CobblepediaFabric implements ModInitializer {

    @Override
    public void onInitialize() {
        Cobblepedia.init();
    }
}
