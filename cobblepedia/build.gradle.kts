import java.io.File
import java.io.FileInputStream
import java.util.*

plugins {
    id("com.modrinth.minotaur") version "2.+"
}

subprojects {
    subprojects {
        repositories {
            // Cobblemon
            maven("https://maven.impactdev.net/repository/development/")
        }

        if(project.name != "common") {
            apply(plugin = "com.modrinth.minotaur")

            val projectResources = rootProject.file("${project.properties["mod_id"]}/${project.properties["minecraft_version"]}/common/src/main/resources")

            tasks.processResources {
                from(projectResources) {
                    include("data/*/patchouli_books/")
                    include("${project.properties["mod_icon"]}")
                    include("${project.properties["mod_logo"]}")
                }
            }

            // local.properties file helper
            val local = Properties().apply {
                load(FileInputStream(File(rootProject.rootDir, "local.properties")))
            }

            val modrinthMinecraftVersionRange = project.property("modrinth_minecraft_version_range") as String
            val isModrinthDebugMode = project.property("modrinth_debug") as String

            modrinth {
                token.set(local.getProperty("MODRINTH_TOKEN")) // Remember to have the MODRINTH_TOKEN environment variable set or else this will fail - just make sure it stays private!
                projectId.set("${project.properties["mod_id"]}") // This can be the project ID or the slug. Either will work!
                versionNumber.set("${project.properties["mod_version"]}") // You don't need to set this manually. Will fail if Modrinth has this version already
                versionName.set("v${project.properties["mod_version"]} - ${project.properties["mod_version_name"]}")
                versionType.set("${project.properties["mod_version_type"]}") // This is the default -- can also be `beta` or `alpha`
                changelog.set("**[Changelog at Gitlab](${project.properties["changelog_page"]})**")
                uploadFile.set(tasks.jar) // With Loom, this MUST be set to `remapJar` instead of `jar`!
                gameVersions.addAll(modrinthMinecraftVersionRange.split(",")) // Must be an array, even with only one version
                dependencies {
                    required.project("cobblemon")
                    required.project("patchouli")
                }
                debugMode.set(isModrinthDebugMode.toBoolean())
                //syncBodyFrom = rootProject.file("cobblepedia/README.md").text
            }
        }
    }
}