# Cobblepedia's Changelog

## v0.5.3 - Polishing and Finishing - 2024-01-24

Added the Trading with Players entry, finished adding text to some textless entries and added some guiding.

**Additions**
- Spotlight to many entries using tags;
- Entry: Trading

**Changes**
- Page flip sound now uses minecraft's click sound;
- Set option `read_by_default` to true for entries inside the Items, Blocks and Commands categories;
- Moved Apricorn Tree image page;
- Added (?) icon to some entries to guide new players;
- Entry: Battling, Battling Other Players

**Fixes**
- Bad references for entries: Apricorns;
- Removed sortnum from some poke ball entries.

## v0.5.2 - Pokémon Category and Fixes - 2023-11-29

**Additions**
- New Pokémon category;
- New entries: Pokémon Unique Forms, Choice Items;

**Changes**
- Moved entries to Pokémon category: Pokémon Drops, Pokémon Spawning, Shiny Pokémon, Shoulder Mount;
- Moved entries to Getting Started category: Machines, Apricorn Wood Set, Evolution Stone Ores;
- Added references to entries: each apricorn, each apricorn sprout, Evolution Stone Mining;
- Added Dripstone Moon Stone Ore to Evolution Stone Ores entry listings;
- Added descriptions to evolution stone items and evolution stone ore blocks;

**Fixes**
- Bad references for entries: Apricorn, Apricorn Seed, Basic Balls;
- Page references changed on entries: Caching, Evolution Stone Mining;
- Entry lang typos for: Apricorn Gence Gate, Azure Ball.

## v0.5.1 - Friends and Farms Hotfix - 2023-11-26

**Fixes**
- Medicine Brew entry typo that causes an error on Patchouli v82+

## v0.5.0 - Friends and Farms Update - 2023-11-10

**Additions**
- Added Categories: Agriculture, Items. Blocks
- Added Entries: 
    - Items: Heal Powder, Revival Herb, Revive, Max Revive, Remedy, Fine Remedy, Suerb Remedy, Berries, Berry Juice, Mulches, Herbs, X Items, PP Up, PP Max, Medicinal Leek, Roasted Leek, Leek and Potato Stew, Vivichoke, Braised Vivichoke, Vivichoke Dip, Power Items, Auspicious Armor, Malicious Armor, Mints, Mint Leaves, Energy Root, Medicinal Brew, Brews
    - Blocks: Pasture Block, Apricorn Set, Pep-up Flower, Vivichoke Seeds, Mint Seeds
    - Consumables: Candy, Vitamin, Medicine, X Item, Food, Mint, Brew
    - Agriculture: Apricorn Sprout, Crop, Berry, Berry Tree, Berry Mutating
    - Evolution Items: Trading Evolution Items, Using Evolution Items, Sweet
    - Held Items: Type Boost Items, Herb, Power Item

**Changes**
- Added Blocks and Items categories and converted some categories into entries that lists other entries
- Removed categories and moved its content to other categories: Apricorn, Machines
- Replaced apricorn farming with apricorn sprout entry
- Moved Big Root to Blocks category
- Updated entries: Vitamins. Big Root

**Fixes**
- Fixed Big Root entry name

**Dev**
- Renamed apricorn_trees entry to apricorn_tree
- Added templates: Spotlight, Natural Berry, Mutated Berry, Brewing
- Cobblepedia changed it's source code base and now is part of the new [Knowlogy Books](https://gitlab.com/VinnyStalck/knowlogy) project.
