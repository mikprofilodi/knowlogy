# Knowlogy Book

This mod adds the **knowlogy book**, that shows the information from the [Minecraft Wiki](https://minecraft.wiki/), that takes in consideration the game's progression by unlocking pages as the player completes achievements. 

It's meant to be a way of showing a lot information, without overwhelming the player with walls of text or giant list of items right from the start of the game, but still making the rich documentation from a wiki to be available when useful.

### Lore

Created by [The Host of Knowledge](https://minecraft.wiki/w/Minecraft_Legends:Knowledge), this magical artifact is able to acquire knowledge from it's environment end share it with who reads it.