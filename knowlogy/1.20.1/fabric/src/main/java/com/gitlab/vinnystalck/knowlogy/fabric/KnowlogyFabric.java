package com.gitlab.vinnystalck.knowlogy.fabric;

import com.gitlab.vinnystalck.knowlogy.Knowlogy;
import net.fabricmc.api.ModInitializer;

/**
 * This class is the entrypoint for the mod on the Fabric platform.
 */
public class KnowlogyFabric implements ModInitializer {

    @Override
    public void onInitialize() {
        Knowlogy.init();
    }
}
