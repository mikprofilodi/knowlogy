package com.gitlab.vinnystalck.knowlogy.forge;

import com.gitlab.vinnystalck.knowlogy.Knowlogy;
import net.minecraftforge.fml.common.Mod;

/**
 * Main class for the mod on the Forge platform.
 */
@Mod(Knowlogy.MOD_ID)
public class KnowlogyForge {
    public KnowlogyForge() {
        Knowlogy.init();
    }
}
