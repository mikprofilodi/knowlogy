subprojects {
    subprojects {
        if(project.name != "common") {
            val projectResources = rootProject.file("${project.properties["mod_id"]}/${project.properties["minecraft_version"]}/common/src/main/resources")

            tasks.processResources {
                from(projectResources) {
                    include("data/*/patchouli_books/")
                    include("${project.properties["mod_icon"]}")
                    include("${project.properties["mod_logo"]}")
                }
            }
        }
    }
}