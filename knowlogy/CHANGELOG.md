# Knowlogy Books's Changelog

## a0.1.0 - First Alfa

**Additions**
- Categories added:
    - Content categories: Items, Blocks, Mobs, Biomes, Structures, Effects, Enchantments, Tutorials, Newcomer Tutorials, Redstone Circuits;
    - Advancement categories: Minecraft, Adventure, Husbandry, Nether and End;
- Entries addded at categories:
    - Items: Armor, Axe, Boots, Chestplate, Helmet, Leggings, Pickaxe, Sword, Tool, Weapon
    - Blocks: Slab, Suspicious Blocks
    - Mobs: Boss Mobs, Hostile Mobs, Neutral Mobs, Passive Mobs
    - Tutorials: Beguinner's Guide
    - Minecraft: Breaking, Crafting, Enchanting, Smelting, Smithing
    - Adventure: Archaeology, Trading, Trimming
    - Husbandry: Animal, Breeding, Taming
    - Nether: Brewing, The Nether, Upgrading
    - End: The End