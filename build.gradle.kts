plugins {
    id("architectury-plugin") version "3.4-SNAPSHOT"
    id("dev.architectury.loom") version "1.4-SNAPSHOT" apply false
    id("java-library")
    idea
    java
}

subprojects {
    allprojects {
        apply(plugin = "java")
        apply(plugin = "architectury-plugin")
        apply(plugin = "maven-publish")
        apply(plugin = "idea")

        tasks.withType<JavaCompile>().configureEach {
            options.encoding = "UTF-8"
            options.release.set(17)
        }

        java.withSourcesJar()
    }
    subprojects {
        subprojects {
            repositories {
                mavenCentral()
                mavenLocal()
                maven("https://maven.parchmentmc.org")
                // Patchouli
                maven("https://maven.blamejared.com")

                maven("https://cursemaven.com").content { includeGroup("curse.maven") }
                maven("https://api.modrinth.com/maven").content { includeGroup("maven.modrinth") }
            }
        }
    }
}

