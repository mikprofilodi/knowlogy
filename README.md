# Knowlogy Books

This is a project aimed to make Minecraft mods that adds well documented wiki-like books to the game in a way that follows it's game design.

To achieve this, the data-driven documentantion mod [Patchouli](https://github.com/VazkiiMods/Patchouli) is used as a dependency to all these mods.

## Mods

### [Knowlogy](https://gitlab.com/VinnyStalck/knowlogy/-/tree/main/cobblepedia:1.20.1)

For minecraft vanilla

### [Cobblepedia](https://gitlab.com/VinnyStalck/knowlogy/-/tree/main/knowlogy:1.20.1)

For the [Cobblemon](https://modrinth.com/mod/cobblemon) mod

## Name

Based on the [Knowledge Book](https://minecraft.wiki/w/Knowledge_Book) item that teaches (or unlocks) recipes to the player, the [Know](https://en.wikipedia.org/wiki/Knowledge)+[-logy](https://en.wikipedia.org/wiki/-logy) is the study that knowledge, the knowlogy book "studies" knowledge and displays it to the player.