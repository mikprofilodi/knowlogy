pluginManagement.repositories {
    maven("https://maven.fabricmc.net/")
    maven("https://maven.architectury.dev/")
    maven("https://maven.minecraftforge.net/")
    gradlePluginPortal()
}

plugins {
    `gradle-enterprise`
}

gradleEnterprise.buildScan {
    termsOfServiceUrl = "https://gradle.com/terms-of-service"
    termsOfServiceAgree = "yes"
}

include(
    "knowlogy",
)

include(
    "knowlogy:1.20.1",
    "knowlogy:1.20.1:common",
    "knowlogy:1.20.1:fabric",
    "knowlogy:1.20.1:forge"
)

include(
    "cobblepedia",
)

include(
    "cobblepedia:1.20.1",
    "cobblepedia:1.20.1:common",
    "cobblepedia:1.20.1:fabric",
    "cobblepedia:1.20.1:forge"
)

rootProject.name = "knowlogy-books"
